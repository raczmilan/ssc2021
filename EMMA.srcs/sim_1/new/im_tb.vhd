----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/03/2022 02:55:19 PM
-- Design Name: 
-- Module Name: im_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity im_tb is
--  Port ( );
end im_tb;

architecture Behavioral of im_tb is

signal clk : STD_LOGIC := '0';
signal rst : STD_LOGIC := '0';
signal control : STD_LOGIC_VECTOR (3 downto 0) := "0000";
signal BUS2  : STD_LOGIC_VECTOR (7 downto 0) := x"00";
signal Instr  : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
constant CLK_PERIOD : TIME := 10 ns;

begin
DUT: entity WORK.IFc port map (
                     clk => clk,
                     en => '1',
                     rst => rst,
                     BUS2 => BUS2,
                     control => control,
                     Instr => Instr);
                     
gen_clk: process
begin
 clk <= '1';
 wait for (CLK_PERIOD/2);
 clk <= '0';
 wait for (CLK_PERIOD/2);
end process gen_clk;

gen_vect_test: process
 begin
 rst <= '1';
 wait for CLK_PERIOD/2;
 rst <= '0';
 wait for CLK_PERIOD/2;
 control <= "0101";
 BUS2 <= x"04";
 wait for CLK_PERIOD;
 BUS2 <= x"fe";
 control <= "0011";
 wait for CLK_PERIOD;
 control <= "0111";
 wait for CLK_PERIOD;
 
 wait;
 
 end process gen_vect_test; 

end Behavioral;
