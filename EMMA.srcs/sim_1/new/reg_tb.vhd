library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg_tb is
--  Port ( );
end reg_tb;

architecture Behavioral of reg_tb is

signal clk : STD_LOGIC := '0';
signal control : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
signal addr : STD_LOGIC_VECTOR (11 downto 0) := x"000";
signal wd  : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
signal rd1 : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
signal rd2 : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
constant CLK_PERIOD : TIME := 10 ns;

begin
DUT: entity WORK.Registers port map (
                     clk => clk,
                     control => control(15 downto 12),
                     addr => addr,
                     wd => wd,
                     rd1 => rd1,
                     rd2 => rd2);
                     
gen_clk: process
begin
 clk <= '0';
 wait for (CLK_PERIOD/2);
 clk <= '1';
 wait for (CLK_PERIOD/2);
end process gen_clk;

gen_vect_test: process
 begin
 addr <= x"A00";
 wd <= x"0000ABCD";
 wait for CLK_PERIOD;
 addr <= x"000";
 wd <= x"0000ABCD";
 wait for CLK_PERIOD;
 addr <= x"B0A";
 wd <= x"00001234";
 wait for CLK_PERIOD;
 addr <= x"0B0";
 wd <= x"00000000";
 
 wait;
 
 end process gen_vect_test; 
 
end Behavioral;
