----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/03/2022 02:55:19 PM
-- Design Name: 
-- Module Name: mu_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mu_tb is
--  Port ( );
end mu_tb;

architecture Behavioral of mu_tb is

signal clk1 : STD_LOGIC := '0';
signal clk2 : STD_LOGIC := '0';
signal rst : STD_LOGIC := '0';
signal CC   : STD_LOGIC_VECTOR (1 downto 0);
signal MPC   : STD_LOGIC_VECTOR (7 downto 0);
signal Instr   : STD_LOGIC_VECTOR (31 downto 0);
signal addr_out   : STD_LOGIC_VECTOR (11 downto 0);
signal MPC_out   : STD_LOGIC_VECTOR (7 downto 0);
signal control : STD_LOGIC_VECTOR (23 downto 0);
signal BUS1 : STD_LOGIC_VECTOR (31 downto 0);
signal BUS2 : STD_LOGIC_VECTOR (31 downto 0);
constant CLK_PERIOD : TIME := 2 ns;

begin
DUT: entity WORK.MU 
          port map (clk1 => clk1,
                    clk2 => clk2,
                    en => '1',
                    rst => rst,
                    CC => CC,
                    MPC => MPC,
                    Instr => Instr,
                    addr_out => addr_out,
                    control => control,
                    MPC_out => MPC_out,
                    BUS1 => BUS1,
                    BUS2 => BUS2);
                     
gen_clk: process
begin
 clk1 <= '1';
 wait for (CLK_PERIOD/2);
 clk1 <= '0';
 wait for (CLK_PERIOD/2);
end process gen_clk;

gen_clk2: process
begin
 clk2 <= '0';
 wait for (CLK_PERIOD/2);
 clk2 <= '1';
 wait for (CLK_PERIOD/2);
end process gen_clk2;

gen_vect_test: process
 begin
 rst<='1';
 CC <= "00";
 Instr <= x"4100000f";
 wait for (CLK_PERIOD/2);
 rst<='0';
 wait for (CLK_PERIOD/2);
 wait for (CLK_PERIOD);
 MPC <= x"2e";
 Instr <= x"42000003";
 wait for (CLK_PERIOD);
 MPC <= x"2f";
 wait for (CLK_PERIOD);
 MPC <= x"2f";
 wait for (CLK_PERIOD);
 MPC <= x"2e";
 Instr <= x"00000000";
 wait for (CLK_PERIOD);
 MPC <= x"2f";
 wait;
 
 end process gen_vect_test;
end Behavioral;
