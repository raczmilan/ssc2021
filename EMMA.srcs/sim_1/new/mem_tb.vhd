----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/30/2021 04:59:34 PM
-- Design Name: 
-- Module Name: mem_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mem_tb is
--  Port ( );
end mem_tb;

architecture Behavioral of mem_tb is

signal clk1 : STD_LOGIC := '0';
signal clk2 : STD_LOGIC := '0';
signal control : STD_LOGIC_VECTOR (3 downto 0) := "0000";
signal MAR_in  : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
signal MBR_in  : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
signal MBR_out : STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
constant CLK_PERIOD : TIME := 10 ns;

begin
DUT: entity WORK.Mem port map (
                     clk1 => clk1,
                     clk2 => clk2,
                     control => control,
                     MAR_in => MAR_in,
                     MBR_in => MBR_in,
                     MBR_out => MBR_out);
                     
gen_clk: process
begin
 clk1 <= '1';
 wait for (CLK_PERIOD/2);
 clk1 <= '0';
 wait for (CLK_PERIOD/2);
end process gen_clk;

gen_clk2: process
begin
 clk2 <= '0';
 wait for (CLK_PERIOD/2);
 clk2 <= '1';
 wait for (CLK_PERIOD/2);
end process gen_clk2;

gen_vect_test: process
 begin
 wait for CLK_PERIOD;
 MAR_in <= x"0000000A";
 MBR_in <= x"0000ABCD";
 control <= "1110";
 wait for CLK_PERIOD;
 MAR_in <= x"0000000B";
 control <= "1001";
 wait for CLK_PERIOD;
 MAR_in <= x"0000000A";
 control <= "1001";
 wait for CLK_PERIOD;
 MAR_in <= x"00000000";
 control <= "1001";
 
 wait;
 
 end process gen_vect_test; 

end Behavioral;
