----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/13/2021 08:07:56 PM
-- Design Name: 
-- Module Name: alu_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu_tb is
--  Port ( );
end alu_tb;

architecture Behavioral of alu_tb is

signal clk1 : STD_LOGIC := '0';
signal clk2 : STD_LOGIC := '0';
signal A_in : STD_LOGIC_VECTOR (31 downto 0);
signal B_in : STD_LOGIC_VECTOR (31 downto 0);
signal control : STD_LOGIC_VECTOR (11 downto 0);
signal CC   : STD_LOGIC_VECTOR (1 downto 0);
signal ACC_BUS : STD_LOGIC_VECTOR (31 downto 0);
signal ACC_REG : STD_LOGIC_VECTOR (31 downto 0);
constant CLK_PERIOD : TIME := 2 ns;

begin
DUT: entity WORK.ALU 
          port map (clk1 => clk1,
                    clk2 => clk2,
                    A_in => A_in,
                    B_in => B_in,
                    control => control,
                    CC => CC,
                    ACC_BUS => ACC_BUS,
                    ACC_REG => ACC_REG);
                     
gen_clk: process
begin
 clk1 <= '1';
 wait for (CLK_PERIOD/2);
 clk1 <= '0';
 wait for (CLK_PERIOD/2);
end process gen_clk;

gen_clk2: process
begin
 clk2 <= '0';
 wait for (CLK_PERIOD/2);
 clk2 <= '1';
 wait for (CLK_PERIOD/2);
end process gen_clk2;

gen_vect_test: process
 begin

 A_in <= x"0000fff0";
 B_in <= x"00000004";
 control <= x"ae1";
 wait for (CLK_PERIOD);
 control <= x"a21";
 wait;
 
 end process gen_vect_test; 


end Behavioral;
