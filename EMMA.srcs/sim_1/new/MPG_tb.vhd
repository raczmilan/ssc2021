----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/02/2021 05:36:52 PM
-- Design Name: 
-- Module Name: MPG_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MPG_tb is
--  Port ( );
end MPG_tb;

architecture Behavioral of MPG_tb is

signal btn  : STD_LOGIC := '0';
signal clk  : STD_LOGIC := '0';
signal clk1 : STD_LOGIC := '0';
signal clk2 : STD_LOGIC := '0';
signal control : STD_LOGIC_VECTOR (3 downto 0) := "0000";

constant CLK_PERIOD : TIME := 2 ns;

begin
DUT: entity WORK.MPG port map (
                     btn => btn,
                     clk => clk,
                     control => control,
                     clk1 => clk1,
                     clk2 => clk2);
                     
gen_clk: process
begin
 clk <= '1';
 wait for (CLK_PERIOD/2);
 clk <= '0';
 wait for (CLK_PERIOD/2);
end process gen_clk;

gen_vect_test: process
 begin
 control <= x"9";
 wait for 10 ns;
 control <= x"5";
 wait for 50 ns;
 btn <= '1';
 wait for 6 ns;
 btn <= '0';
 wait for 52 ns;
 btn <= '1';
 wait for 10 ns;
 btn <= '0';
 
 
 wait;
 
 end process gen_vect_test; 

end Behavioral;

