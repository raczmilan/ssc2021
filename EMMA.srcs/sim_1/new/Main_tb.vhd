----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2021 01:36:54 PM
-- Design Name: 
-- Module Name: Main_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Main_tb is
--  Port ( );
end Main_tb;

architecture Behavioral of Main_tb is

signal btn    : STD_LOGIC := '0';
signal clk    : STD_LOGIC := '0';
signal ALU_out : STD_LOGIC_VECTOR (31 downto 0);
signal BUS1    : STD_LOGIC_VECTOR (31 downto 0);
signal BUS2    : STD_LOGIC_VECTOR (31 downto 0);
signal control : STD_LOGIC_VECTOR (23 downto 0);
signal MPC_out : STD_LOGIC_VECTOR (7 downto 0);
signal CC_out : STD_LOGIC_VECTOR (1 downto 0);
signal Instr   : STD_LOGIC_VECTOR (31 downto 0);
constant CLK_PERIOD : TIME := 6 ns;

begin
                     
gen_clk: process
begin
 clk <= '1';
 wait for (CLK_PERIOD/2);
 clk <= '0';
 wait for (CLK_PERIOD/2);
end process gen_clk;

gen_vect_test: process
 begin
 
 wait for 80 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 170 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 170 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 170 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 170 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 560 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 170 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 170 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait for 180 ns;
 btn <= '1';
 wait for 20 ns;
 btn <= '0';
 wait;
 
 end process gen_vect_test; 


end Behavioral;
