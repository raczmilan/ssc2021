library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Registers is
    Port ( clk : in STD_LOGIC;
           control: in STD_LOGIC_VECTOR(3 downto 0);
           addr : in STD_LOGIC_VECTOR (11 downto 0);
           wd : in STD_LOGIC_VECTOR (31 downto 0);
           rd1 : out STD_LOGIC_VECTOR (31 downto 0);
           rd2 : out STD_LOGIC_VECTOR (31 downto 0));
end Registers;

architecture Behavioral of Registers is
    
    type reg_array is array(0 to 15) of std_logic_vector(31 downto 0);
    signal reg_file : reg_array := (others => x"00000000");
    
begin

    process(clk)
    begin
        if falling_edge(clk) then
            if control(2) = '1' and conv_integer(addr(11 downto 8)) /= 0 then
                reg_file(conv_integer(addr(11 downto 8))) <= wd;
            end if;
        end if;
    end process;
    
    process(addr, control)
    begin
        if control(1 downto 0) = "11" then
            rd1 <= reg_file(conv_integer(addr(7 downto 4)));
            rd2 <= reg_file(conv_integer(addr(3 downto 0)));
        elsif control(1 downto 0) = "10" then
            rd1 <= reg_file(conv_integer(addr(7 downto 4)));
            rd2 <= reg_file(0);
        elsif control(1 downto 0) = "01" then
            rd1 <= reg_file(0);
            rd2 <= reg_file(conv_integer(addr(3 downto 0)));
        else
            rd1 <= reg_file(0);
            rd2 <= reg_file(0);
        end if;
    end process;
end Behavioral;
