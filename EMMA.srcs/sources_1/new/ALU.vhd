library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ALU is
    Port ( clk1     : in STD_LOGIC;
           clk2     : in STD_LOGIC;
           rst      : in STD_LOGIC;
           A_in     : in STD_LOGIC_VECTOR (31 downto 0);
           B_in     : in STD_LOGIC_VECTOR (31 downto 0);
           control  : in STD_LOGIC_VECTOR (11 downto 0);
           CC       : out STD_LOGIC_VECTOR (1 downto 0);
           ACC_BUS  : out STD_LOGIC_VECTOR (31 downto 0);
           ACC_REG  : out STD_LOGIC_VECTOR (31 downto 0));
end ALU;

architecture Behavioral of ALU is

    type t is array (0 to 31) of std_logic_vector(31 downto 0);
    signal A        : std_logic_vector(31 downto 0) := (others => '0');
    signal B        : std_logic_vector(31 downto 0) := (others => '0');
    signal ACC      : std_logic_vector(31 downto 0) := (others => '0');
    signal res      : std_logic_vector(31 downto 0) := (others => '0');
    signal add      : std_logic_vector(31 downto 0) := (others => '0');
    signal sub      : std_logic_vector(31 downto 0) := (others => '0');
    signal add1     : std_logic_vector(31 downto 0) := (others => '0');
    signal sub1     : std_logic_vector(31 downto 0) := (others => '0');
    signal addmul   : std_logic_vector(31 downto 0) := (others => '0');
    signal submul   : std_logic_vector(31 downto 0) := (others => '0');
    signal subdiv   : std_logic_vector(31 downto 0) := (others => '0');
    signal and_op   : std_logic_vector(31 downto 0) := (others => '0');
    signal or_op    : std_logic_vector(31 downto 0) := (others => '0');
    signal xor_op   : std_logic_vector(31 downto 0) := (others => '0');
    signal sll_op   : t;
    signal srl_op   : t;
    signal sra_op   : t;
    signal noop     : std_logic_vector(31 downto 0) := (others => '0');
    signal rsub     : std_logic_vector(31 downto 0) := (others => '0');
    signal nega     : std_logic_vector(31 downto 0) := (others => '0');
    signal negb     : std_logic_vector(31 downto 0) := (others => '0');
    signal shra     : std_logic_vector(31 downto 0) := (others => '0');
    signal shla     : std_logic_vector(31 downto 0) := (others => '0');
    signal shrb     : std_logic_vector(31 downto 0) := (others => '0');
    signal shlb     : std_logic_vector(31 downto 0) := (others => '0');
    signal setd     : std_logic_vector(31 downto 0) := (others => '0');
    signal decrd    : std_logic_vector(31 downto 0) := (others => '0');
    signal sh_d     : std_logic_vector(31 downto 0) := (others => '0');
    signal D        : std_logic_vector(31 downto 0) := (others => '0');
    signal err      : std_logic := '0';
    signal nresult  : std_logic := '0';
    signal C        : std_logic := '1';
    
    signal   Stare  : STD_LOGIC_VECTOR (3 downto 0) := "0000"; 
    constant adds   : STD_LOGIC_VECTOR (3 downto 0) := "0000"; 
    constant subs   : STD_LOGIC_VECTOR (3 downto 0) := "0001"; 
    constant ands   : STD_LOGIC_VECTOR (3 downto 0) := "0010"; 
    constant ors    : STD_LOGIC_VECTOR (3 downto 0) := "0011"; 
    constant xors   : STD_LOGIC_VECTOR (3 downto 0) := "0100";
    constant slls   : STD_LOGIC_VECTOR (3 downto 0) := "0101";
    constant srls   : STD_LOGIC_VECTOR (3 downto 0) := "0110";
    constant sras   : STD_LOGIC_VECTOR (3 downto 0) := "0111";
    constant noops  : STD_LOGIC_VECTOR (3 downto 0) := "1000";
    constant rsubs  : STD_LOGIC_VECTOR (3 downto 0) := "1001";
    constant negas  : STD_LOGIC_VECTOR (3 downto 0) := "1010";
    constant negbs  : STD_LOGIC_VECTOR (3 downto 0) := "1011";
    constant shas   : STD_LOGIC_VECTOR (3 downto 0) := "1100";
    constant shbs   : STD_LOGIC_VECTOR (3 downto 0) := "1101";
    constant setds  : STD_LOGIC_VECTOR (3 downto 0) := "1110";
    constant decrds : STD_LOGIC_VECTOR (3 downto 0) := "1111";
    
begin
    process (clk1)
    begin
         if rising_edge(clk1) then
            ACC <= res;
            if control(7 downto 6) = "11" then -- initializari
                ACC <= (others => '0');
                err <= '0';
                nresult <= '0';
                if control(3 downto 0) = adds and control(11 downto 8) = "1010" then -- inmultire
                    if B_in(31 downto 16) /= 0 or A_in(31 downto 16) /= 0 then
                        err <= '1';
                    else
                        A <= A_in;
                        B <= B_in;
                        if A_in(31) = '1' then
                            Stare <= negas;
                        else
                            Stare <= adds;
                        end if;
                    end if;
                elsif control(3 downto 0) = subs and control(11 downto 8) = "1010" then -- impartire
                    if B_in = 0 or B_in(31 downto 16) /= 0 or A_in(31 downto 16) /= 0 then
                        err <= '1';
                    else
                        A <= (others => '0');
                        B <= B_in;
                        ACC <= A_in;
                        Stare <= setds;
                    end if;
                elsif control(11 downto 8) = "0000" then -- alte operatii
                    A <= A_in;
                    B <= B_in;
                    Stare <= control(3 downto 0);
                end if;
            else
                if control(11 downto 8) = "1010" then
                    if control(3 downto 0) = adds then -- inmultire
                        if Stare = negas then
                            nresult <= '1';
                            A <= nega;
                            Stare <= adds;
                        elsif Stare = adds then
                            Stare <= shas;
                        elsif Stare = shas then
                            A <= shra;
                            Stare <= shbs;
                        elsif Stare = shbs then
                            B <= shlb;
                            Stare <= adds;
                        end if;
                    elsif control(3 downto 0) = subs then --impartire
                        if Stare = setds then
                            D <= x"00000010";
                            if A_in(15) = '1' then
                                Stare <= negas;
                            elsif B_in(15) = '1' then
                                Stare <= negbs;
                            else
                                B <= B(15 downto 0) & x"0000";
                                Stare <= subs;
                            end if;
                        elsif Stare = negas then
                            nresult <= not nresult;
                            ACC <= x"0000" & nega(15 downto 0);
                            if B_in(15) = '1' then
                                Stare <= negbs;
                            else
                                B <= B(15 downto 0) & x"0000";
                                Stare <= subs;
                            end if;
                        elsif Stare = negbs then
                            nresult <= not nresult;
                            B <= negb(15 downto 0) & x"0000";
                            Stare <= subs;
                        elsif Stare = subs then
                            Stare <= shas;
                        elsif Stare = shas then
                            A <= shla;
                            if C = '1' then
                                Stare <= shbs;
                            else
                                Stare <= rsubs;
                            end if;
                        elsif Stare = rsubs then
                            Stare <= shbs;
                        elsif Stare = shbs then
                            B <= shrb;
                            Stare <= decrds;
                        elsif Stare = decrds then
                            if D > 0 then
                                D <= D - 1;
                                Stare <= subs;
                            end if;
                        end if;
                    elsif control(11 downto 8) = "0000" then -- alte operatii
                        Stare <= control(3 downto 0);
                    end if;
                end if;
            end if;
         end if;
    end process;
    
    process (clk2)
    begin
         if rising_edge(clk2) then
             if rst = '1' then
                CC <= "00";
             elsif control(11 downto 8) = "0000" then -- alte operatii
                if err = '1' then
                    CC <= "10";
                elsif res = 0 then
                    CC <= "00";
                elsif res(31) = '1' then
                    CC <= "11";
                elsif res(31) = '0' then
                    CC <= "01";
                end if;
             elsif control(3 downto 0) = adds and control(11 downto 8) = "1010" and Stare = shas then -- inmultire
                if err = '1' then
                    CC <= "10";
                elsif A = 0 then
                    CC <= "00";
                elsif A(31) = '1' then
                    CC <= "11";
                elsif A(31) = '0' then
                    CC <= "01";
                end if;
             elsif control(3 downto 0) = subs then -- impartire
                if err = '1' then
                    CC <= "10";
                elsif D = 0 and Stare = decrds then
                    CC <= "00";
                elsif res(31) = '1' then
                    CC <= "11";
                    C <= '0';
                elsif res(31) = '0' then
                    CC <= "01";
                    C <= '1';
                end if;  
             end if;            
         end if;
    end process;
    
    process (clk2)
    begin
        if rising_edge(clk2) then
             if rst = '1' then
                ACC_BUS <= (others => '0');
                ACC_REG <= (others => '0');
             elsif control(11 downto 8) = "0000" or control(11 downto 8) = "1001" then
                if control(5 downto 4) = "11" then
                    ACC_BUS <= res;
                    ACC_REG <= res;
                elsif control(5 downto 4) = "01" then
                    ACC_BUS <= res;
                    ACC_REG <= (others => '0');
                elsif control(5 downto 4) = "10" then
                    ACC_REG <= res;
                    ACC_BUS <= (others => '0');
                else
                    --ACC_REG <= (others => '0');
                    ACC_BUS <= (others => '0');
                end if;
             elsif control(11 downto 8) = "1010" then
                if control(5 downto 4) = "11" then
                    if control(3 downto 0) = adds then
                        if nresult = '1' then
                            ACC_BUS <= 0 - res;
                            ACC_REG <= 0 - res;
                        else
                            ACC_BUS <= res;
                            ACC_REG <= res;
                        end if;
                    elsif control(3 downto 0) = subs then
                        if nresult = '1' then
                            ACC_BUS <= (0 - A(15 downto 0)) & (0 - res(15 downto 0));
                            ACC_REG <= (0 - A(15 downto 0)) & (0 - res(15 downto 0));
                        else
                            ACC_BUS <= A(15 downto 0) & res(15 downto 0);
                            ACC_REG <= A(15 downto 0) & res(15 downto 0);
                        end if;
                    end if;
                elsif control(5 downto 4) = "01" then
                    if control(3 downto 0) = adds then
                        if nresult = '1' then
                            ACC_BUS <= 0 - res;
                        else
                            ACC_BUS <= res;
                        end if;
                    elsif control(3 downto 0) = subs then
                        if nresult = '1' then
                            ACC_BUS <= (0 - A(15 downto 0)) & (0 - res(15 downto 0));
                        else
                            ACC_BUS <= A(15 downto 0) & res(15 downto 0);
                        end if;
                    end if;
                elsif control(5 downto 4) = "10" then
                    if control(3 downto 0) = adds then
                        if nresult = '1' then
                            ACC_REG <= 0 - res;
                            ACC_BUS <= (others => '0');
                        else
                            ACC_REG <= res;
                            ACC_BUS <= (others => '0');
                        end if;
                    elsif control(3 downto 0) = subs then
                        if nresult = '1' then
                            ACC_REG <= (0 - A(15 downto 0)) & (0 - res(15 downto 0));
                            ACC_BUS <= (others => '0');
                        else
                            ACC_REG <= A(15 downto 0) & res(15 downto 0);
                            ACC_BUS <= (others => '0');
                        end if;
                    end if;
                end if;
             end if;
         end if;
    end process;
    
    process(control, ACC, A, addmul, submul, subdiv, add1, sub1)
        begin
        if control(11 downto 8) = "1010" then
            if control(3 downto 0) = adds and A(0) = '1' then -- inmultire 
                add <= addmul;
                sub <= submul;
            elsif control(3 downto 0) = adds and A(0) = '0' then -- inmultire
                add <= ACC;
                sub <= ACC;
            elsif control(3 downto 0) = subs then -- impartire
                add <= (others => '0');
                sub <= subdiv;
            end if;
        else -- alte operatii
            add <= add1;
            sub <= sub1;
        end if;
    end process;
    
    add1 <= A + B;
    sub1 <= A - B;
    addmul <= ACC + B;
    submul <= ACC - B;
    subdiv <= ACC - B;
    and_op <= A and B;
    or_op <= A or B;
    xor_op <= A xor B;
    sll_op(0) <= A;
    srl_op(0) <= A;
    sra_op(0) <= A;   
    gen: for i in 1 to 31 generate
         sll_op(i) <= sll_op(i-1)(30 downto 0) & "0";
         srl_op(i) <= "0" & srl_op(i-1)(31 downto 1);
         sra_op(i) <= sra_op(i-1)(31) & sra_op(i-1)(31 downto 1);
    end generate gen;
    nega <= 0 - A_in;
    negb <= 0 - B_in;
    shra <= '0' & A(31 downto 1);
    shla <= A(30 downto 0) & C;
    shrb <= '0' & B(31 downto 1);
    shlb <= B(30 downto 0) & '0';
    rsub <= ACC + B;
        
    process (Stare, add, sub, sll_op, srl_op, and_op, or_op, sra_op, xor_op, noop, rsub, A_in, ACC)
    begin
         case Stare is
             when adds   => res <= add;     
             when subs   => res <= sub;
             when ands   => res <= and_op;
             when ors    => res <= or_op;
             when xors   => res <= xor_op;
             when slls   => res <= sll_op(conv_integer(B(4 downto 0)));
             when srls   => res <= srl_op(conv_integer(B(4 downto 0)));
             when sras   => res <= sra_op(conv_integer(B(4 downto 0)));
             when noops  => res <= noop;
             when rsubs  => res <= rsub;
             when others => res <= ACC;
         end case;
    end process;

end Behavioral;
