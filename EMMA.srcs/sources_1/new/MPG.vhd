----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/02/2021 05:24:39 PM
-- Design Name: 
-- Module Name: MPG - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MPG_en is
    Port ( btn : in STD_LOGIC;
           clk : in STD_LOGIC;
           control : in STD_LOGIC_VECTOR(3 downto 0);
	       enable : out STD_LOGIC);
end MPG_en;

architecture Behavioral of MPG_en is

signal q1 : std_logic := '0';
signal q2 : std_logic := '0';
signal q3 : std_logic := '0';
signal en : std_logic := '0';
signal sig : std_logic_vector(3 downto 0) := x"0";

begin

enable <= en;

process (clk)
begin

    if rising_edge(clk) then
        sig <= sig + 1;
        q2 <= q1;
        q3 <= q2;
        if sig = "1111" then
            q1 <= btn;
        end if;
    end if;
    
end process;

en <= (q2 and not q3) or (control(3) and control(0));

end Behavioral;