----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/27/2021 01:22:24 PM
-- Design Name: 
-- Module Name: MU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MU is
    Port ( clk1    : in STD_LOGIC;
           clk2    : in STD_LOGIC;
           en      : in STD_LOGIC;
           rst     : in STD_LOGIC;
           CC      : in STD_LOGIC_VECTOR (1 downto 0);
           MPC     : in STD_LOGIC_VECTOR (7 downto 0);
           Instr   : in STD_LOGIC_VECTOR (31 downto 0);
           addr_out: out STD_LOGIC_VECTOR (11 downto 0);
           control : out STD_LOGIC_VECTOR (23 downto 0);
           MPC_out : out STD_LOGIC_VECTOR (7 downto 0);
           BUS1    : out STD_LOGIC_VECTOR (31 downto 0);
           BUS2    : out STD_LOGIC_VECTOR (31 downto 0));
end MU;

architecture Behavioral of MU is

type mem_type is array (0 to 255) of std_logic_vector(39 downto 0);
    signal mem: mem_type := (
    x"10000000_00", --0
    x"10353000_19", --1 add
    x"10353001_1b", --2 sub
    x"10353002_1d", --3 and
    x"10353003_1f", --4 or
    x"10353004_21", --5 xor
    x"10353005_23", --6 sll
    x"10353006_25", --7 srl
    x"10353007_27", --8 sra
    ------------------------
    x"30352000_29", --9 addl
    x"30352001_2b", --10 subl
    ------------------------
    x"10351900_2d", --11 ld
    x"30350000_2e", --12 ldl
    x"30352000_30", --13 ldx
    x"10353e00_33", --14 st
    x"50351000_34", --15 stx
    ------------------------
    x"20930000_00", --16 jump
    x"00931000_00", --17 jreg
    x"3a370000_36", --18 beqz
    x"36370000_37", --19 bneg
    ------------------------
    x"10353000_38", --20 mul
    x"30352000_39", --21 mull
    x"10353000_3c", --22 div
    x"30352000_3d", --23 divl
    x"10000000_00", --24 stop
    ------------------------
    x"103930e0_1a", --25 add
    x"00994000_00", --26 add
    x"103930e1_1c", --27 sub
    x"00994001_00", --28 sub
    x"103930e2_1e", --29 and
    x"00994002_00", --30 and
    x"103930e3_20", --31 or
    x"00994003_00", --32 or
    x"103930e4_22", --33 xor
    x"00994004_00", --34 xor
    x"103930e5_24", --35 sll
    x"00994005_00", --36 sll
    x"103930e6_26", --37 srl
    x"00994006_00", --38 srl
    x"103930e7_28", --39 sra
    x"00994007_00", --40 sra
    ------------------------
    x"303920e0_2a", --41 addl
    x"00994000_00", --42 addl
    x"303920e1_2c", --43 subl
    x"00994001_00", --44 subl
    ------------------------
    x"00994900_00", --45 ld
    x"303900e0_2f", --46 ldl
    x"00994000_00", --47 ldl
    x"303920d0_31", --48 ldx
    x"10390910_32", --49 ldx
    x"00994900_00", --50 ldx
    x"00990e00_00", --51 st
    x"503910d0_35", --52 stx
    x"00992e00_00", --53 stx
    ------------------------
    x"00990000_00", --54 beqz
    x"00990000_00", --55 bneg
    ------------------------
    x"10393ae0_3a", --56 mul
    x"30392ae0_3a", --57 mull
    x"10393a20_3b", --58 mul
    x"10394a20_00", --59 mul
    x"10393ae1_3e", --60 div
    x"30392ae1_3e", --61 divl
    x"10393a21_3f", --62 div
    x"10394a21_00", --63 div
    ------------------------
    others => x"00000000_00");
    
    signal MPC_addr   : std_logic_vector(7 downto 0) := x"00";
    signal B1         : std_logic_vector(31 downto 0) := x"00000000";
    signal B2         : std_logic_vector(31 downto 0) := x"00000000";
    signal IR         : std_logic_vector(31 downto 0) := x"00000000";
    signal addr       : std_logic_vector(11 downto 0) := x"000";
    signal mcode      : std_logic_vector(39 downto 0) := x"00000000_00";
    

begin
    
    MPC_out <= MPC_addr;
    BUS1 <= B1;
    BUS2 <= B2;
    addr_out <= addr;
    
    process (clk1)
    begin
        if rising_edge(clk1) then
            if rst = '1' then
                mcode <= (others => '0');
                addr <= (others => '0');
            elsif en = '1' then
                if mcode(7 downto 0) = x"00" then
                    if IR(31 downto 28) = "0000" then
                        mcode <= mem(conv_integer(IR(15 downto 12)) + 1);
                    else    
                        mcode <= mem(conv_integer(IR(31 downto 28)) + 8);
                    end if;
                    if IR(31 downto 28) = "0011" then
                        addr <= IR(27 downto 24) & "0000" & IR(23 downto 20);
                    elsif IR(31 downto 28) = "0110" or IR(31 downto 28) = "0111" then
                        addr <= "0000" & IR(23 downto 20) & IR(27 downto 24);
                    elsif IR(31 downto 28) = "1001" then
                        addr <= "0000" & "0000" & IR(27 downto 24);
                    else
                        addr <= IR(27 downto 16);
                    end if;
                elsif ((mcode(7 downto 0) = x"3b" or mcode(7 downto 0) = x"3f") and CC = "00") or (mcode(7 downto 0) /= x"3b" and mcode(7 downto 0) /= x"3f") then
                    mcode <= mem(conv_integer(MPC));
                end if;
            end if;
        end if;
    end process;

    process (clk2)
    begin
        if rising_edge(clk2) then
            control <= mcode(31 downto 8);
            if mcode(38) = '1' then
                B1 <= x"0000" & IR(15 downto 0);
            else
                B1 <= (others => '0');
            end if;
            if mcode(37) = '1' then
                B2 <= x"0000" & IR(15 downto 0);
            else
                B2 <= (others => '0');
            end if;
            if mcode(36) = '1' then
                MPC_addr <= mcode(7 downto 0);
            end if;
            if (mcode(35 downto 32) = "1010" and CC(0) = '1') or (mcode(35 downto 32) = "0110" and CC(1) = '0') then
                control <= mcode(31 downto 28) & "0101" & mcode(23 downto 8);
            else
                control <= mcode(31 downto 8);
            end if;
        end if;
    end process;
    
    process (clk2)
    begin
        if rising_edge(clk2) and en = '1' then
            IR <= Instr;
    end if;
    end process;

end Behavioral;
