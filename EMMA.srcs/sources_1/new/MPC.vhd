----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/28/2021 03:52:09 PM
-- Design Name: 
-- Module Name: MPC - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MPC is
    Port ( clk      : in STD_LOGIC;
           mcode    : in STD_LOGIC_VECTOR (7 downto 0);
           control  : in STD_LOGIC_VECTOR (3 downto 0);
           out_addr : out STD_LOGIC_VECTOR (7 downto 0));
end MPC;

architecture Behavioral of MPC is

signal addr : std_logic_vector(7 downto 0) := (others => '0');
signal sig : std_logic_vector(7 downto 0) := (others => '0');

begin

sig <= mcode;

process (clk)
begin
    if rising_edge(clk) then
        if control = "1001" then
            out_addr <= addr;
        elsif control = "0011" then
            addr <= sig;
            out_addr <= sig;
        elsif control = "0010" then
            addr <= sig;
        end if;
    end if;
end process;

end Behavioral;
