library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Mem is
    Port ( clk1    : in STD_LOGIC;
           clk2    : in STD_LOGIC;
           control : in STD_LOGIC_VECTOR (3 downto 0);
           MAR_in  : in STD_LOGIC_VECTOR (31 downto 0);
           MBR_in  : in STD_LOGIC_VECTOR (31 downto 0);
           MBR_out : out STD_LOGIC_VECTOR (31 downto 0));
end Mem;

architecture Behavioral of Mem is

    type mem_type is array (0 to 127) of STD_LOGIC_VECTOR(31 downto 0);
    signal mem   : mem_type := (
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000002",
    x"00000000",
    x"00000000",
    others => x"00000000");
    signal MAR   : STD_LOGIC_VECTOR(31 downto 0) := x"00000000";
    signal MBR   : STD_LOGIC_VECTOR(31 downto 0) := x"00000000";
    
    
begin

process(clk1)
begin
    if rising_edge(clk1) then
        if control(3 downto 0) = "1001" then
            MAR <= MAR_in;
            MBR <= mem(conv_integer(MAR_in));
        elsif control(3 downto 0) = "1110" then
            MAR <= MAR_in;
            MBR <= MBR_in;
        end if;
    end if;
end process;

process(clk2)
begin
    if rising_edge(clk2) then
        if control(3 downto 0) = "1001" then
            MBR_out <= MBR;
        elsif control(3 downto 0) = "1110" then
            mem(conv_integer(MAR)) <= MBR;
            MBR_out <= (others => '0');
        else
            MBR_out <= (others => '0');
        end if;
    end if;
end process;

end Behavioral;
