----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/28/2021 04:09:19 PM
-- Design Name: 
-- Module Name: Main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Main is
    Port ( btn  : in STD_LOGIC;
           reset  : in STD_LOGIC;
           clk  : in STD_LOGIC;
           sw   : in STD_LOGIC_VECTOR (7 downto 0);
           An   : out STD_LOGIC_VECTOR (7 downto 0);
           Seg  : out STD_LOGIC_VECTOR (7 downto 0));    
end Main;

architecture Behavioral of Main is

signal clk1     : STD_LOGIC := '0';
signal clk2     : STD_LOGIC := '1';
signal btns     : STD_LOGIC := '0';
signal en       : STD_LOGIC := '0';
signal rst      : STD_LOGIC := '0';
signal CC       : STD_LOGIC_VECTOR (1 downto 0) := (others => '0');
signal MPC_addr : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
signal to_MPC   : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
signal control  : STD_LOGIC_VECTOR (23 downto 0) := (others => '0');
signal addr     : STD_LOGIC_VECTOR (11 downto 0) := (others => '0');
signal wd       : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal BUS1     : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal BUS2     : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal ALU_out  : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal Instr    : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal Data     : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

signal B11      : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal B21      : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal B13      : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal B12      : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal B22      : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal B23      : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

begin

process (sw, ALU_out, CC, Instr, control, BUS1, BUS2, MPC_addr, wd)
        begin
           case sw(7 downto 5) is
               when "000" => Data <= ALU_out;
               when "001" => Data <= x"0000000" & "00" & CC;
               when "010" => Data <= BUS1;
               when "011" => Data <= BUS2;
               when "100" => Data <= Instr;
               when "101" => Data <= x"00" & control;
               when "110" => Data <= x"000000" & MPC_addr;
               when others => Data <= wd;
           end case;
        end process;
        
btns <= reset or btn;

BUS1 <= (B11 or B12) or B13;
BUS2 <= (B21 or B22) or B23;

process(control, BUS1, ALU_out)
begin
    if control(11 downto 8) = "1001" then
        wd <= BUS1;
    else
        wd <= ALU_out;
    end if;
end process;

MU: entity WORK.MU  
          port map (clk1 => clk1,
                    clk2 => clk2,
                    en => en,
                    rst => rst,
                    CC => CC,
                    MPC => MPC_addr,
                    Instr => Instr,
                    addr_out => addr,
                    control => control,
                    MPC_out => to_MPC,
                    BUS1 => B11,
                    BUS2 => B21);
                    
Instr_mem: entity WORK.Ifc
          port map (clk => clk1,
                    en => en,
                    rst => rst,
                    BUS2 => BUS2(7 downto 0),
                    control => control(19 downto 16),
                    Instr => Instr);
                    
MPC: entity WORK.MPC  
          port map (clk => clk,
                    mcode => to_MPC,
                    control => control(23 downto 20),
                    out_addr => MPC_addr);

Registers: entity WORK.Registers 
          port map (clk => clk2,
                    control => control(15 downto 12),
                    addr => addr,
                    wd => wd,
                    rd1 => B12,
                    rd2 => B22);

ALU: entity WORK.ALU
          port map (clk1 => clk1,
                    clk2 => clk2,
                    rst => rst,
                    A_in => BUS1,
                    B_in => BUS2,
                    control => control(11 downto 0),
                    CC => CC,
                    ACC_BUS => B23, 
                    ACC_REG => ALU_out);
                    
MEM: entity WORK.Mem
          port map (clk1 => clk1,
                    clk2 => clk2,
                    control => control(11 downto 8),
                    MAR_in => BUS2,
                    MBR_in => BUS1,
                    MBR_out => B13);
                    
MPG_en: entity WORK.MPG_en
          port map (btn => btns,
                    clk => clk,
                    control => control(19 downto 16),
                    enable => en);
                    
MPG_rst: entity WORK.MPG_rst
          port map (btn => reset,
                    clk => clk,
                    control => control(19 downto 16),
                    rst => rst);
                    
clk_gen: entity WORK.Clk_gen
          port map (clk => clk,
                    clk1 => clk1,
                    clk2 => clk2);
                    
display: entity WORK.displ7seg
          port map (Clk => clk,
                    Rst => rst,
                    Data => Data,
                    An => An,
                    Seg => Seg);

end Behavioral;
