----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2021 03:09:08 PM
-- Design Name: 
-- Module Name: IM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IFc is
    Port ( clk      : in STD_LOGIC;
           en       : in STD_LOGIC;
           rst      : in STD_LOGIC;
           BUS2     : in STD_LOGIC_VECTOR (7 downto 0);
           control  : in STD_LOGIC_VECTOR (3 downto 0);
           Instr    : out STD_LOGIC_VECTOR (31 downto 0));
end IFc;

architecture Behavioral of IFc is

type mem_type is array (0 to 255) of std_logic_vector(31 downto 0);
    signal mem: mem_type := (
    x"4100000f", -- LDL R1, 15
    x"42000003", -- LDL R2, 3
    x"f3100004", -- DIVL R3, R1, 4
    x"11100001", -- ADDL R1, R1, 1
    x"03317000", -- SRA R3, R3, R1
    x"05230000", -- ADD R5, R2, R3
    others => x"00000000");

signal PC : std_logic_vector(7 downto 0) := (others => '0');
signal addr : std_logic_vector(7 downto 0) := (others => '0');
signal new_addr : std_logic_vector(7 downto 0) := (others => '0');
signal sig1 : std_logic_vector(7 downto 0) := (others => '0');
signal sig2 : std_logic_vector(7 downto 0) := (others => '0');
signal sig3 : std_logic_vector(7 downto 0) := (others => '0');

begin

sig1 <= PC + 1;
sig2 <= BUS2;
sig3 <= PC + BUS2;

process (clk)
begin
    if rising_edge(clk) and en = '1' then
        if rst = '1' then
            PC <= x"00";
            addr <= x"00";
        elsif control = "1001" then
            addr <= PC;
        elsif control = "0101" then
            PC <= sig1;
            addr <= sig1;
        elsif control = "0011" then
            PC <= sig2;
            addr <= sig2;
        elsif control = "0111" then
            PC <= sig3;
            addr <= sig3;
        elsif control = "0100" then
            PC <= sig1;
        elsif control = "0010" then
            PC <= sig2;
        elsif control = "0110" then
            PC <= sig3;
        end if;
    end if;
end process;


Instr <= mem(conv_integer(addr));

end Behavioral;
