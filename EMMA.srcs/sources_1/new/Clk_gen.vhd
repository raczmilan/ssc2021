----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/11/2021 08:20:19 PM
-- Design Name: 
-- Module Name: Clk_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Clk_gen is
    Port ( clk : in STD_LOGIC;
           clk1 : out STD_LOGIC;
           clk2 : out STD_LOGIC);
end Clk_gen;

architecture Behavioral of Clk_gen is

begin

clk1 <= clk;
clk2 <= not clk;

end Behavioral;
